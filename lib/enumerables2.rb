require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0,:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|phrase| sub_string?(phrase, substring)}
end

def sub_string?(phrase, word)
  phrase.split.include?(word)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  characters = uniq_letters_only(string)
  characters.reduce([]) do |repeat_letters, chr|
    string.count(chr) > 1 ? repeat_letters << chr : repeat_letters
  end.sort
end

def uniq_letters_only(string)
  alpha = ('a'..'z').to_a
  string.chars.uniq.select {|chr| alpha.include?(chr)}
end


# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string_array = string.split
  return string_array if string_array.length < 3

  length_of_words = string_array.map(&:length).sort[-2]
  string_array.select {|word| word.length >= length_of_words}
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alpha = ('a'..'z').to_a

  alpha - string.chars.uniq
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?

def one_week_wonders(songs)
  delete = []
  songs.each_with_index {|song, idx| delete << song if songs[idx] == songs[idx+1]}
  songs.select {|song| !delete.include?(song)}.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string
  .split
  .map {|word| word = remove_punctuation(word)}
  .reduce do |closest_c, word|
    c_distance(closest_c) > c_distance(word) ?
    word : closest_c
  end
end

def c_distance(word)
  return 100 if !word.include?("c")
  word.reverse.index('c')
end

def remove_punctuation(word)
  alphabet = ('a'..'z').to_a
  word.chars.delete_if {|chr| !alphabet.include?(chr)}.join
end


# p for_cs_sake("acc!?!?...,, lock")
p for_cs_sake("toe sticker ack lock box")

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeated_range = []
  start_idx = nil

  arr.each_with_index do |num, idx|
    next_num = arr[idx+1]
      if num == next_num
        start_idx = idx unless start_idx
      elsif start_idx
        repeated_range << [start_idx, idx]
        start_idx = nil
      end
  end
  repeated_range
end
